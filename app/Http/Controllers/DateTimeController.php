<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class DateTimeController extends Controller
{
    public function dateHeader()
    {
        $fdate  = "2019-11-18";
        $tdate  = "2019-11-30";
        $period = 4; 
        $header = '';
        $arr_date = [];
        
        for($i = 0; $i < $period; $i++){
            if($i==0){
                $header.='<th>'.$i.'</th>';
                $search_date = ['fdate'=>$fdate,'tdate'=>$tdate];
            }else{
                $header.='<th>'.$i.'</th>';
                $search_date = ['fdate'=>$fdate,'tdate'=>$tdate];
            }
            $arr_date[$i] = $search_date;
            
        }
        
        $result = ['header'=>$header,'arr_date'=>$arr_date];
        echo '<pre>';
        var_dump($result);
        echo '</pre>';
        
        $table ='<table border=1>
                    <thead>
                        <th>No</th>
                        <th>Payment Method</th>
                        '.$result['header'].'
                    </thead>
                </table>';
        return $table;
    }
}
